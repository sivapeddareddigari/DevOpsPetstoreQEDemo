package ca.gc.ircc.ta.wrappers;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.JMSContext;

import com.codeborne.selenide.Condition;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.SkipException;


import ca.gc.ircc.ta.buildingblocks.ReadTestdata;
import ca.gc.ircc.ta.buildingblocks.RemoteConnectUtils;
import ca.gc.ircc.ta.buildingblocks.RestAssuredUtil;
import io.restassured.response.Response;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.*;


public class petstoremaster {

    public static final String pattern = "dd/MM/yyyy HH:mm:ss SS";
    public static final SimpleDateFormat format = new SimpleDateFormat(pattern);
    final static Logger logger = Logger.getLogger(petstoremaster.class);

    public String hostname;
    public int port;
    public String username;
    public String password;

    public JMSContext queueConnectionContext;
    public Destination dataSendToQueue;
    RemoteConnectUtils fcutils = new RemoteConnectUtils();
    ReadTestdata rt = new ReadTestdata();
    RestAssuredUtil rut = new RestAssuredUtil();
    Response res;
    String BaseURI;
    String BasePath;
    String ScenarioDescription;
    String SearchBoxText;
    String browser;
    String Positive;

    long Sdate;
    long FDate;
    final static Logger perf = Logger.getLogger("perf");
    final static Logger file = Logger.getLogger("file");
    boolean scenarioStatus = true;

    @Parameters({ "ScenarioName", "TestDataFileName" })

    @BeforeClass
    public void BootStrap(String ScenarioName, String TestDataFileName) {

        try {

            BasicConfigurator.configure();
            HashMap<String, String> ScenarioData = new HashMap<String, String>();
            String log4jConfPath = "resources/log4j.properties";
            PropertyConfigurator.configure(log4jConfPath);

            Sdate = System.currentTimeMillis();

            if (TestDataFileName.contains(".xlsx")) {
                XSSFWorkbook workbook;
                FileInputStream finput = null;
                XSSFSheet sheet;
                File src = new File(TestDataFileName);

                try {
                    finput = new FileInputStream(src);

                    System.out.println(".....finput initialized");

                    workbook = new XSSFWorkbook(finput);
                    System.out.println(".....Workbook initialized");
                    sheet = workbook.getSheet("Initial");
                    System.out.println(".....initial Sheet initialized");

                    ScenarioData = rt.getScenarioData(workbook, sheet, ScenarioName);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (TestDataFileName.contains(".JSON")) {
                try {
                    ScenarioData = rt.readInputFromJSON("Queue", TestDataFileName, ScenarioName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            BaseURI = ScenarioData.get("BaseURI");
            BasePath = ScenarioData.get("BasePath");
            SearchBoxText = ScenarioData.get("SearchBoxText");
            browser = ScenarioData.get("browser");
            Positive = ScenarioData.get("Positive");
            ScenarioDescription = ScenarioData.get("ScenarioDescription");
            Configuration.browser= browser;

            logger.info("Scenarios which is Running ::" + ScenarioDescription);

            logger.info("Requesting URI: " + BaseURI+ BasePath + "...");

            logger.info("...Responded");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public void Afterclassfunctionhere() {
        closeWebDriver();
        if (scenarioStatus) {

            FDate = System.currentTimeMillis();
            System.err.println("total time taken ~:" + (FDate - Sdate));
            perf.info(
                     ScenarioDescription + " | " + this.getClass().getName() + " | " + (FDate - Sdate));
        }

    }

    @Test(priority = 1, enabled = true)
    public void userCanLoginByUsername() {


        open("https://www.google.com");
        $(By.name("q")).setValue(SearchBoxText);
        $(By.name("btnK")).submit();

       $(By.id("logo")).shouldHave(appear);


    }

    @Test(priority = 2, enabled = true)
    public void CheckResponseContent() {



    }

    @Test(priority = 3, enabled = true)
    public void validateAtSource() {


    }

}
